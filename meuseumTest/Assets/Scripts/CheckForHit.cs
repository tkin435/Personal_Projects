﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckForHit : MonoBehaviour
{
	public static CheckForHit instance;
	public Text txt;
	public AudioClip audioClip;
	public AudioSource audioSource;
	//For the telepoortation
	public Transform player;
	public string cameraTeloportTag = "TeleportCube";
	public Vector3 teloportOffset = Vector3.zero;
	//click on a object
	//display a text
	public RaycastHit hit;
	public Image bar;
	private Coroutine corGazeToggle;

	private void Awake() {
		instance = this;
		bar.gameObject.SetActive(false);
	}

	private void Start()
	{
		audioSource.clip = audioClip;
	}

	private void Update() {
		CheckforHit();
	}

	public void CheckforHit()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		int layerMask = 1 << 8;

		Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask);
		if (hit.transform && hit.transform.tag == "Interactable") {
			Interactable interactable = hit.collider.gameObject.GetComponent<Interactable>();
			if (interactable && corGazeToggle == null) {
				corGazeToggle = StartCoroutine(GazeToggle(interactable));
			}
		} else if (corGazeToggle != null) {
			StopCoroutine(corGazeToggle);
			corGazeToggle = null;
			bar.gameObject.SetActive(false);
		}
	}

	private IEnumerator GazeToggle(Interactable _interactable) {
		float t = 0;
		float duration = 2.0f;
		Color hehe = Color.black;

		bar.gameObject.SetActive(true);

		while (t < duration) {
			//hehe = Color.Lerp(Color.red, Color.clear, fraction);
		
			float fraction = t / duration;
			bar.fillAmount = Mathf.Lerp(1.0f, 0.0f, fraction);

			t += Time.deltaTime;
			yield return null;
		}

		_interactable.Teleport();

		corGazeToggle = null;
	}




	//if (hitting)
	//{
	//    Debug.DrawLine(ray.origin, hit.point, Color.green);
	//    Debug.Log("Its hittin");

	//    //TODO: Place this statement where to the specific object not in this method
	//    if (hit.transform.tag == "Cylinder")
	//    {
	//        txt.text = "A solid bounded by a cylindrical surface and two parallel planes is called a (solid) cylinder . ... The height (or altitude) of a cylinder is the perpendicular distance between its bases. The cylinder obtained by rotating a line segment about a fixed line that it is parallel to is a cylinder of revolution .";

	//    }
	//    else if (hit.transform.tag == "Cube")
	//    {
	//        txt.text = "In geometry, a cube is a three-dimensional solid object bounded by six square faces, facets or sides, with three meeting at each vertex. The cube is the only regular hexahedron and is one of the five Platonic solids.";
	//    }
	//    else if (hit.transform.tag == "Sphere")
	//    {
	//        txt.text = "A sphere is a perfectly round geometrical object in three-dimensional space that is the surface of a completely round ball";            }

	//    audioSource.Play();


	//    raycastHitObject = hit.collider.gameObject;

	//    while (raycastHitObject.tag == cameraTeloportTag)
	//    {
	//        Debug.Log("asdasdasd");

	//        player.localPosition = hit.transform.position;
	//        return;
	//    }
	//}
	//else
	//{
	//    Debug.DrawLine(ray.origin, hit.point, Color.blue);
	//    Debug.Log("Not hitting");
	//    txt.text = "Welcome to the meuseum";

	//}
}