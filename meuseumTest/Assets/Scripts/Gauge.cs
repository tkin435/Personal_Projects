﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Gauge : MonoBehaviour {
    public Image currentPoint;
    public Text ratioText;

    private float point = 10;
    private float maxpoint = 10;

	// Use this for initialization
	void Start () {
        UpdateGaugeBar();
	}

    private void UpdateGaugeBar()
    {
        float ratio = point / maxpoint;
        currentPoint.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString();
    }

    private void ReduceGauge(float reduce)
    {
        point -= reduce;
        if(point < 0)
        {
            point = 0;
        }
        UpdateGaugeBar();
    }

    private void RegainGauge(float gain )
    {
        point += gain;
        if(point > maxpoint)
        {
            point = maxpoint;
        }
        UpdateGaugeBar();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
